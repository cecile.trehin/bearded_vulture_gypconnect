#
#     ANALYSE GYPA : RSF to estimate seasonal habitat selection during the first year post-fledging  #### 
#     @C.Trehin 2023
#___________________________________________________________________________



#Chargement des packages
#-----------------------

library(sp)
library(spdep)
library(rgdal)
library(raster)
library(dismo)
library(adehabitatLT)
library(adehabitatHR)
library(adehabitatHS)
library(XML)
library(lubridate)
library(ggplot2)
library(lattice)
library(rgeos)
library(geosphere)
library(segclust2d)
library(anytime)
library(plotrix)
library(car)
library(nnet)
library(SparkR)
library(scales)
library(cowplot)
library(SDMTools)
library(mvnormtest)
library(amt)
library(stringr)
library(dplyr)
library(ResourceSelection)
library(glmmTMB)
library(INLA)
library(dplyr)

setwd("~/Gypa/Stage Gypa M2/Scripts R")
facteurs=read.table("facteurs.txt",sep="\t",header = T)

setwd("~/Gypa/Stage Gypa M2/Scripts R/Sauvegardes_espaces_de_travail")
load("data_list_kernelUD_saison_fus.RData")
load("data_saison_fus_contours.RData")


setwd("~/Gypa/Stage Gypa M2/SIG_gypa/CLC_reclass")
clc_reclass=raster("clc_250_RGF93_reclass_SFS.tif")

individus=names(list_spdf_1h_RGF93_HR2)

Aude = 1
Baronnies = 2:6
Causses = 7:18
Corse = 19:24
HteSavoie = 25:28
Mercantour= 29:34
Pyrenees = 35
Vercors= 36:45

col=rep(NA,44)
col[Aude]=1
col[Baronnies]=2
col[Causses]=3
col[Corse]=4
col[HteSavoie]=5
col[Mercantour]=6
col[Pyrenees]=7
col[Vercors]=8

sites <- NA
sites[1] <- "Aude"
sites[2:6] <- "Baronnies"
sites[7:18] <- "Causses"
sites[19:24] <- "Corse"
sites[25:28] <- "HteSavoie"
sites[29:34] <- "Mercantour"
sites[35] <- "Pyrenees"
sites[36:45] <- "Vercors"

pop <- sites
pop[pop %in% c("Aude","Pyrenees","Mercantour","HteSavoie")] <- "Mountains"
pop[pop %in% c("Baronnies","Vercors")] <- "Pre-Alps"




# TEST RFS SUR 1 INDIVIDU
#____________________________


crs(list_spdf_1h_RGF93_HR2[[1]])


select <- list_spdf_1h_RGF93_HR2[[1]][which(list_spdf_1h_RGF93_HR2[[1]]$id_saison == "DV1-Automne 1"),]
points=raster::intersect(select,list_HR_contours_saison[[1]]$Kernel95[1,])
plot(points) 
plot(list_HR_contours_saison[[1]]$Kernel95[1,])

proj <- CRS(as.character("+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"))
traj <- mk_track(points,.x = x.1, .y = y.1, .t = date, crs = proj)
crs(traj)
plot(traj)

r1 <- random_points(traj)
plot(r1)

r1 <- random_points(traj, n = nrow(traj))
plot(r1)


hr <- list_HR_contours_saison[[1]]$Kernel95[1,]
plot(hr)
r1 <- random_points(hr, n = nrow(traj))
plot(r1)


r1 <- random_points(hr, n = nrow(traj), presence = traj)
plot(r1)



rsf1 <- r1 %>% extract_covariates(clc_reclass)
rsf1$habitat <- NA
rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 1] <- "autres"
rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 2] <- "milieux_fermes"
rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 3] <- "milieux_ouverts"
rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 4] <- "placettes"


rsf1 %>% fit_rsf(case_ ~ habitat) %>% 
  summary()





#BOUCLE SUR TOUS LES INDIVIDUS ET TOUTES LES SAISONS
#_____________________________________________________
 

# liste pour stocker les resultats de tous les individus
list_rsf1 <- list()
list_HR_contours_saison[[9]]$Kernel95 <- list_HR_contours_saison[[9]]$Kernel90 #vérifier pourquoi y'a pas le kernel 95!!


for (i in 1:length(individus)){
  if(!is.na(list_spdf_1h_RGF93_HR2[[i]])){
  
  print(i)
  # liste pour stocker les resultats pour l'individu i 
  list_rsf1_ind <- list()
  # saisons à considérer pour l'individu i
  saisons <- unique(list_spdf_1h_RGF93_HR2[[i]]$id_saison)
  
  for(j in 1:length(saisons)){
    # sélectionner les points correspondant à l'individu pour la saison j
    select <- list_spdf_1h_RGF93_HR2[[i]][which(list_spdf_1h_RGF93_HR2[[i]]$id_saison == saisons[j]),]  
    # sélectionner le domaine vital correspondant à l'individu pour la saison j
    hr <- list_HR_contours_saison[[i]]$Kernel95[list_HR_contours_saison[[i]]$Kernel95$id == saisons[j],]
    # intersection entre les points et le domaine vital
    points <- raster::intersect(select,hr)
    # points en formats trajectoire (format nécessaire pour la rsf)
    proj <- CRS(as.character("+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"))
    traj <- mk_track(points,.x = x.1, .y = y.1, .t = date, crs = proj)
    # créer des points aléatoires
    r1 <- random_points(hr, n = nrow(traj)*10, presence = traj)
    # extraire le type d'habitat pour chaque point
    rsf1 <- r1 %>% extract_covariates(clc_reclass)
    rsf1$habitat <- NA
    rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 1] <- "autres"
    rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 2] <- "milieux_fermes"
    rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 3] <- "milieux_ouverts"
    rsf1$habitat[rsf1$clc_250_RGF93_reclass_SFS == 4] <- "placettes"
    
    rsf1$habitat <- factor(rsf1$habitat, levels = c("milieux_fermes","milieux_ouverts","placettes","autres"))
    
    rsf1 <- na.omit(rsf1)
    # fit du modèle rsf
    rsf1 %>% fit_rsf(case_ ~ habitat) %>% 
      summary() -> summary_rsf1
    
    #stocker le tableau avec toutes les saisons pour individu i
    rsf <- rsf1
    rsf$ID <- individus[i]
    rsf$pop <- pop[i]
    rsf$site <- sites[i]
    rsf$saison <- saisons[j]
    rsf$sexe <- facteurs$Sexe[i]
    rsf$origin <- facteurs$Statut.lacher[i]
    if(j == 1){rsf_i <- rsf}else{rsf_i <- rbind(rsf_i,rsf)}
    
    # stockage des resultats pour chaque saison j pour l'individu i
    list_rsf1_ind[[j]] <- summary_rsf1
    names(list_rsf1_ind[j]) <- saisons[j]
    
    df <- data.frame(pop = pop[i],
                     site = sites[i],
                     ind = individus[i],
                     saison = saisons[j],
                     sexe = facteurs$Sexe[i],
                     origin = facteurs$Statut.lacher[i],
                     habitat =row.names(summary_rsf1$coefficients),
                     estimate = summary_rsf1$coefficients[1:nrow(summary_rsf1$coefficients),1],
                     p_value = summary_rsf1$coefficients[1:nrow(summary_rsf1$coefficients),4])
    
    df$habitat[df$habitat=="(Intercept)"] <- "habitatmilieux_fermes"
    row.names(df) <- NULL
    
    if(j == 1){df_ind <- df}else{df_ind <- rbind(df_ind, df)}
  }
  # stockage des resultats pour toutes les saisons de l'individu i
  list_rsf1[[i]] <- list_rsf1_ind
  names(list_rsf1[i]) <- individus[i]
  if(i == 1){df_tot <- df_ind}else{df_tot <- rbind(df_tot,df_ind)}
  if(i == 1){rsf_tot <- rsf_i}else{rsf_tot <- rbind(rsf_tot,rsf_i)}
  
  }


}


df_tot$saison2 <- str_sub(df_tot$saison,5)
df_tot$habitat<- str_sub(df_tot$habitat,8)

df_subset <- subset(df_tot,df_tot$saison2 %in% c("Ete 1","Automne 1","Hiver 1","Printemps 2","Ete 2" ))
df_subset$saison2 <- factor(df_subset$saison2, levels = c("milieux_fermes","milieux_ouverts","placettes","autres" ))

ggplot(data=df_subset,aes(y=estimate, x = habitat))+
  facet_grid(pop~saison2)+
  geom_point(color="blue",shape=4)+
  geom_boxplot()
  

#write.csv(df_subset[order(df_subset$pop),], file = "bilan_rsf_individus.csv")


df_subset %>% group_by(pop,saison2,habitat) %>% 
  summarise(est_mean = mean(estimate, na.rm=T), 
            est_sd = sd(estimate, na.rm=T),
            N = n()) -> bilan

ggplot(data=bilan,aes(x=est_mean, y = habitat))+
  facet_grid(pop~saison2)+
  geom_point(color="blue")+
  geom_errorbar( aes( xmin=est_mean-est_sd, xmax=est_mean+est_sd), width=.2,
                 position=position_dodge(.9),color = "blue")+
  geom_vline(xintercept = 0, col = "grey")


ggplot(data=bilan,aes(x=est_mean, y = habitat))+
  facet_grid(pop~saison2)+
  geom_point(color="blue")+
  geom_vline(xintercept = 0, col = "grey")


bilan2 <- bilan
bilan2$saison2 <- factor(bilan2$saison2, levels = c("Ete 2","Printemps 2","Hiver 1","Automne 1","Ete 1" ))
bilan2$habitat <- factor(bilan2$habitat , levels = c("milieux_ouverts","placettes","milieux_fermes","autres" ))

  
ggplot(data=bilan2,aes(x=est_mean, y =saison2))+
  facet_grid(habitat~pop)+
  geom_point(color="blue")+
  geom_errorbar( aes( xmin=est_mean-est_sd, xmax=est_mean+est_sd), width=.2,
                 position=position_dodge(.9),color = "blue")+
  geom_vline(xintercept = 0, col = "grey")





# Mise en forme table résultats
h1 <- subset(bilan, bilan$habitat == "milieux_ouverts")
h1$milieux_ouverts <- paste(round(h1$est_mean,2)," (",round(h1$est_sd,2),")",sep="")
h1 <- h1[-c(3,4,5)]

h2 <- subset(bilan[c("N","est_mean","est_sd")], bilan$habitat == "milieux_fermes")
h2$milieux_fermes <- paste(round(h2$est_mean,2)," (",round(h2$est_sd,2),")",sep="")
h2 <- h2[-c(2,3)]

h3 <- subset(bilan[c("N","est_mean","est_sd")], bilan$habitat == "placettes")
h3$placettes <- paste(round(h3$est_mean,2)," (",round(h3$est_sd,2),")",sep="")
h3 <- h3[-c(2,3)]

h4 <- subset(bilan[c("N","est_mean","est_sd")], bilan$habitat == "autres")
h4$autres <- paste(round(h4$est_mean,2)," (",round(h4$est_sd,2),")",sep="")
h4 <- h4[-c(2,3)]

bilan2 <- cbind(h1,h2,h3,h4)

#write.csv(bilan2, file = "bilan_rsf.csv")


# MODELES FINAUX AVEC GLMTMB
#_____________________________________________________


# REVIEW3
#-----------------------------------------------------------------------------------------------------------------------------

rsf_tot
rsf_tot$saison2 <- str_sub(rsf_tot$saison,5)
rsf_tot <- subset(rsf_tot,rsf_tot$saison2 %in% c("Ete 1","Automne 1","Hiver 1","Printemps 2","Ete 2" ))
rsf_tot$saison2 <- factor(rsf_tot$saison2, levels = c("Ete 1","Automne 1","Hiver 1","Printemps 2","Ete 2"))
rsf_tot$pop <- factor(rsf_tot$pop, levels = c("Mountains","Causses","Corse","Pre-Alps"))

rsf_tot_new <- rsf_tot[-which(rsf_tot$habitat == "autres"),]


#-----------------------------------

res_tot_tot <- glmmTMB(case_ ~ habitat+ habitat:saison2 + (1|ID) + (1|pop), family=binomial(), data = rsf_tot_new)
sum_tot_tot <- summary(res_tot_tot)
sum_tot_tot

Anova(res_tot_tot)

#----------------------------------------------------------------------------------------------------------------------------------



#######################################

### Metapopulation/ Entire tracking period


new_data <- unique(rsf_tot[c("habitat","saison2","ID","pop","sexe")])

new_data <- new_data[-which(new_data$habitat == "autres"),]

predictions <- predict(res_tot_tot, newdata = new_data, se.fit = T, type = c("response") )
predictions2 <- predict(res_tot_tot, newdata = new_data, se.fit = T )

new_data$fit <- predictions$fit
new_data$se.fit <- predictions$se.fit

new_data$fit_log <- predictions2$fit
new_data$se.fit_log <- predictions2$se.fit


n_ind_saison <- table(rsf_tot$ID,rsf_tot$saison2)
n_ind_saison[n_ind_saison>0] <- 1

n_ind_pop <- table(rsf_tot$ID,rsf_tot$pop)
n_ind_pop[n_ind_pop>0] <- 1

row_names <- rownames(sum_tot_tot$coefficients$cond)

table_rsf_totale <- data.frame(
  season = c("Entire tracking period", levels(rsf_tot$saison2)),
  n = c(length(unique(rsf_tot$ID)),apply(n_ind_saison,2,sum)), 
  milieux_ouverts = c(paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_ouverts")]),3)," (",
                            round(mean(new_data$se.fit[which(new_data$habitat == "milieux_ouverts")]),3),")",sep=""),
                      paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Ete 1")]),3)," (",
                            round(mean(new_data$se.fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Ete 1")]),3),")",sep=""),
                      paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Automne 1")]),3)," (",
                            round(mean(new_data$se.fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Automne 1")]),3),")",sep=""),
                      paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Hiver 1")]),3)," (",
                            round(mean(new_data$se.fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Hiver 1")]),3),")",sep=""),
                      paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Printemps 2")]),3)," (",
                            round(mean(new_data$se.fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Printemps 2")]),3),")",sep=""),
                      paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Ete 2")]),3)," (",
                            round(mean(new_data$se.fit[which(new_data$habitat == "milieux_ouverts" & new_data$saison2 == "Ete 2")]),3),")",sep="")),
  pval1 = c(NA,
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_ouverts"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_ouverts:saison2Automne 1"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_ouverts:saison2Hiver 1"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_ouverts:saison2Printemps 2"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_ouverts:saison2Ete 2"),4],3)),
  milieux_fermes = c(paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_fermes")]),3)," (",
                           round(mean(new_data$se.fit[which(new_data$habitat == "milieux_fermes")]),3),")",sep=""),
                     paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Ete 1")]),3)," (",
                           round(mean(new_data$se.fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Ete 1")]),3),")",sep=""),
                     paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Automne 1")]),3)," (",
                           round(mean(new_data$se.fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Automne 1")]),3),")",sep=""),
                     paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Hiver 1")]),3)," (",
                           round(mean(new_data$se.fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Hiver 1")]),3),")",sep=""),
                     paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Printemps 2")]),3)," (",
                           round(mean(new_data$se.fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Printemps 2")]),3),")",sep=""),
                     paste(round(mean(new_data$fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Ete 2")]),3)," (",
                           round(mean(new_data$se.fit[which(new_data$habitat == "milieux_fermes" & new_data$saison2 == "Ete 2")]),3),")",sep="")),
  pval2 = c(NA,
            round(sum_tot_tot$coefficients$cond[which(row_names == "(Intercept)"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_fermes:saison2Automne 1"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_fermes:saison2Hiver 1"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_fermes:saison2Printemps 2"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatmilieux_fermes:saison2Ete 2"),4],3)),
  placettes = c(paste(round(mean(new_data$fit[which(new_data$habitat == "placettes")]),3)," (",
                      round(mean(new_data$se.fit[which(new_data$habitat == "placettes")]),3),")",sep=""),
                paste(round(mean(new_data$fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Ete 1")]),3)," (",
                      round(mean(new_data$se.fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Ete 1")]),3),")",sep=""),
                paste(round(mean(new_data$fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Automne 1")]),3)," (",
                      round(mean(new_data$se.fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Automne 1")]),3),")",sep=""),
                paste(round(mean(new_data$fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Hiver 1")]),3)," (",
                      round(mean(new_data$se.fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Hiver 1")]),3),")",sep=""),
                paste(round(mean(new_data$fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Printemps 2")]),3)," (",
                      round(mean(new_data$se.fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Printemps 2")]),3),")",sep=""),
                paste(round(mean(new_data$fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Ete 2")]),3)," (",
                      round(mean(new_data$se.fit[which(new_data$habitat == "placettes" & new_data$saison2 == "Ete 2")]),3),")",sep="")),
  pval3 = c(NA,
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatplacettes"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatplacettes:saison2Automne 1"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatplacettes:saison2Hiver 1"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatplacettes:saison2Printemps 2"),4],3),
            round(sum_tot_tot$coefficients$cond[which(row_names == "habitatplacettes:saison2Ete 2"),4],3))
  
)

table_rsf_totale




new_data %>%
  group_by(habitat,saison2,pop) %>%
  summarise(mean_fit = mean(fit_log), sd_fit = sd(fit_log)) -> data_group
data_group


# Graph

new_data$pop <- factor(new_data$pop, levels = c("Causses","Pre-Alps","Mountains","Corse"))

new_data$saison <- NA
new_data$saison[which(new_data$saison2 == "Ete 1")] <- "Summer.1"
new_data$saison[which(new_data$saison2 == "Automne 1")] <- "Autumn.1"
new_data$saison[which(new_data$saison2 == "Hiver 1")] <- "Winter.1"
new_data$saison[which(new_data$saison2 == "Printemps 2")] <- "Spring.1"
new_data$saison[which(new_data$saison2 == "Ete 2")] <- "Summer.2"

new_data$saison <- factor(new_data$saison, levels = c("Summer.1","Autumn.1","Winter.1","Spring.1","Summer.2"))

new_data$habitat2 <- NA
new_data$habitat2[new_data$habitat == "milieux_fermes"] <- "Closed habitats"
new_data$habitat2[new_data$habitat == "milieux_ouverts"] <- "Open habitats"
new_data$habitat2[new_data$habitat == "placettes"] <- "SFS"

#probability scale
ggplot(data= new_data, aes(y=fit, x=pop, fill=pop,color=pop))+
  geom_boxplot(width=0.5,alpha = 0.4)+
  facet_grid(habitat2~saison)+
  labs(y="predicted probability of selection")+
  scale_fill_manual(values=c("red","green3","blue","purple"))+
  scale_color_manual(values=c("red","green3","blue","purple"))+
  theme(axis.title.x = element_blank(),
        legend.title = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_text(size = 12),
        axis.text.y = element_text(size=11),
        legend.text = element_text(size=11),
        strip.text = element_text(size=11))


#logit scale
ggplot(data= new_data, aes(y=fit_log, x=pop, fill=pop,color=pop))+
  geom_boxplot(width=0.5,alpha = 0.4)+
  facet_grid(habitat2~saison)+
  labs(y="selection coefficient")+
  scale_fill_manual(values=c("red","green3","blue","purple"))+
  scale_color_manual(values=c("red","green3","blue","purple"))+
  theme(axis.title.x = element_blank(),
        legend.title = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_text(size = 12),
        axis.text.y = element_text(size=11),
        legend.text = element_text(size=11),
        strip.text = element_text(size=11))


write.table(table_rsf_totale,file="~/Gypa/soumission Vdef Ecosphere/resoumission/table_rsf_totale_121223.txt",sep="\t",row.names = F)






#différences entre sexes?


ggplot(data= new_data, aes(y=fit_log, x=sexe, fill=sexe,color=sexe))+
  geom_boxplot(width=0.5,alpha = 0.4)+
  facet_grid(habitat2~saison)+
  labs(y="selection coefficient")+
  scale_fill_manual(values=c("red","blue"))+
  scale_color_manual(values=c("red","blue"))+
  theme(axis.title.x = element_blank(),
        legend.title = element_blank(),
        axis.text.x = element_blank(),
        axis.title.y = element_text(size = 12),
        axis.text.y = element_text(size=11),
        legend.text = element_text(size=11),
        strip.text = element_text(size=11))


Anova(lm(new_data$fit_log ~ new_data$sexe + new_data$pop))
summary(lm(new_data$fit_log ~ new_data$sexe + new_data$pop))






















