#
#     ANALYSE GYPA : Figures movement phenology article   
#     @C.Trehin 2018
#___________________________________________________________________________




#Chargement data script test phenology
#---------------------------------------

setwd("~/Gypa/Stage Gypa M2/Scripts R/Scripts")
source("script_tests_phenologie.R")


#Figures
#--------


#charge script_tests_phenologie3_trad
slices=as.data.frame(table(tab_Expl_Resi2$Statut1ans))
slices=slices[-c(3:5),]
slices$Var1=factor(slices$Var1,levels=c("Resident","Explorateur intermediaire","Grand explorateur"))
slices=slices[order(slices$Var1),]
slices$colors=c("green2","gold","brown1")
names(slices)=c("statut","Freq","colors")
lbls = as.character(slices$statut)
pct <- round(slices$Freq/sum(slices$Freq)*100)
lbls <- paste(lbls, pct) # add percents to labels 
lbls <- paste(lbls,"%",sep="") # ad % to labels 
pie2<- ggplot(slices, aes(x="", y=Freq, fill=statut))+
  geom_bar(width = 1, stat = "identity")+
  coord_polar("y", start=0)+
  theme(axis.text.x=element_blank(),axis.text.y=element_blank(),axis.title.x=element_blank(),axis.title.y=element_blank(),
        axis.line.x=element_blank(),axis.line.y=element_blank(),plot.title = element_text(size=20),legend.text = element_text(size=20),
        legend.title = element_blank())+
  scale_fill_manual(values=slices$colors )+
  geom_text(aes(x = 1.3, y = cumsum(rev(Freq)) - rev(Freq)/2 ,label = percent(rev(Freq)/sum(Freq))), size=8)+
  labs(title="Proportions ? 1 an")
pie2








# considéerer les NA-Statut comme Statut tout court
tab_Expl_Resi3 = tab_Expl_Resi2

tab_Expl_Resi3$Statut1ans[which(tab_Expl_Resi3$Statut1ans=="NA-Resident")] = NA
tab_Expl_Resi3$Statut1ans[which(tab_Expl_Resi3$Statut1ans=="NA-Explorateur intermediaire")] = "Explorateur intermediaire"
tab_Expl_Resi3$Statut1ans[which(tab_Expl_Resi3$Statut1ans=="NA-Grand explorateur")] = "Grand explorateur"

tab_Expl_Resi3$Statut0.5ans[which(tab_Expl_Resi3$Statut0.5ans=="NA-Resident")] = NA
tab_Expl_Resi3$Statut0.5ans[which(tab_Expl_Resi3$Statut0.5ans=="NA-Explorateur intermediaire")] = "Explorateur intermediaire"
tab_Expl_Resi3$Statut0.5ans[which(tab_Expl_Resi3$Statut0.5ans=="NA-Grand explorateur")] = "Grand explorateur"

tab_Expl_Resi3$Statut1.5ans[which(tab_Expl_Resi3$Statut1.5ans=="NA-Resident")] = NA
tab_Expl_Resi3$Statut1.5ans[which(tab_Expl_Resi3$Statut1.5ans=="NA-Explorateur intermediaire")] = "Explorateur intermediaire"
tab_Expl_Resi3$Statut1.5ans[which(tab_Expl_Resi3$Statut1.5ans=="NA-Grand explorateur")] = "Grand explorateur"

slices1=as.data.frame(table(tab_Expl_Resi3$Statut0.5ans))
slices1=slices1[-c(3:4),]
slices1$pct=round(slices1$Freq/sum(slices1$Freq)*100)
slices1$pct[3]=slices1$pct[3]+1

slices2=as.data.frame(table(tab_Expl_Resi3$Statut1ans))
slices2=slices2[-c(3:5),]
slices2$pct=round(slices2$Freq/sum(slices2$Freq)*100)

slices3=as.data.frame(table(tab_Expl_Resi3$Statut1.5ans))
slices3=slices3[-c(3:4),]
slices3$pct=round(slices3$Freq/sum(slices3$Freq)*100)


slices=rbind(rbind(slices1,slices2),slices3)
slices$period = c(rep("0-0.5 year",3),rep("0.5-1 year",3),rep("1-1.5 year",3))

slices$Var1 = as.character(slices$Var1)
slices$Var1 = rep(c("Medium-dist.","Long-dist.","Sedentary"),3)
slices$Var1 = factor(slices$Var1,levels=c("Sedentary","Medium-dist.","Long-dist."))

slices$colors = rep(c("grey50","grey70","grey30"),3)

names(slices)[1] = "statut"
names(slices)[2] = "n"


slices$lbls = paste(slices$pct,"%",sep="")


pie3<- ggplot(slices, aes(x=period, y=n, fill=statut))+
  geom_bar(width = 0.5, stat = "identity")+
  theme(legend.title = element_blank(),plot.title = element_text(size=13,hjust=0),legend.position = "none",legend.text = element_text(size=10),
        axis.text.x=element_text(angle=45,hjust=0.5,vjust=0.5,size=11),
        axis.title.x = element_blank(), axis.title.y = element_text(size=12))+labs(y="Number of individuals")+
        
  scale_fill_manual(values=c("chartreuse3","orange","red") )+
  #geom_text(aes(x = 1.3, y = cumsum(rev(Freq)) - rev(Freq)/2 ,label = percent(rev(Freq)/sum(Freq))), size=8)+
  geom_text(aes(x = 1, y = 1 ,label = slices$lbls[2]), size=4,color="white")+
  geom_text(aes(x = 1, y = 3 ,label = slices$lbls[1]), size=4,color="white")+
  geom_text(aes(x = 1, y = 20 ,label = slices$lbls[3]), size=4,color="white")+
  geom_text(aes(x = 2, y = 6 ,label = slices$lbls[5]), size=4,color="white")+
  geom_text(aes(x = 2, y = 16 ,label = slices$lbls[4]), size=4,color="white")+
  geom_text(aes(x = 2, y = 22 ,label = slices$lbls[6]), size=4,color="white")+
  geom_text(aes(x = 3, y = 5 ,label = slices$lbls[8]), size=4,color="white")+
  geom_text(aes(x = 3, y = 11 ,label = slices$lbls[7]), size=4,color="white")+
  geom_text(aes(x = 3, y = 14 ,label = slices$lbls[9]), size=4,color="white")+
  
  labs(title="Movement categories")
pie3






dates_df$statut[which(dates_df$statut=="Great.explo")]="Long-dist."
dates_df$statut[which(dates_df$statut=="Inter.explo")]="Medium-dist."

plot2=ggplot(dates_df, aes(date, fill = statut)) + 
  #annotate("rect", xmin=date_median2, xmax=date_medianplus6mois, ymin=-Inf, ymax=Inf, fill="peachpuff", alpha=0.5)+
  #annotate("rect", xmin=date_medianplus6mois, xmax=date_medianplus1an, ymin=-Inf, ymax=Inf, fill="lavenderblush3", alpha=0.5)+
  #annotate("text", x=date_median2, y=3, label="date m?diane ? d'envol",angle = 90,col="dimgray",size=3.5)+
  #annotate("text", x=date_medianplus6mois, y=3, label="date m?diane ? 6 mois",angle = 90,col="dimgray",size=3.5)+
  #annotate("text", x=date_medianplus1an, y=3, label="date m?diane ? 1 an",angle = 90,col="dimgray",size=3.5)+
  geom_histogram(alpha = 1, aes(y = ..count..), position = 'identity',binwidth = 30,center=as.Date("0002-04-15")) +
  geom_vline(xintercept=as.numeric(med1), linetype="dashed", color = "black") + 
  geom_vline(xintercept=as.numeric(med2), linetype="dashed", color = "black")+ 
  scale_x_date(breaks = date_breaks('2 months'),
               labels = date_format("%b"),
               limits = c(as.Date('0001-6-1'), as.Date('0002-7-1'))) +
  theme(axis.text.x=element_text(angle=45,hjust=0.5,vjust=0.5,size=11),axis.title.x=element_blank(),plot.title = element_text(size=13,hjust = 0),
        legend.position = "none",legend.title = element_blank(),legend.text=element_text(size=10),axis.title.y = element_blank())+
  labs(x = "Age post-envol (jours)",title="  Departure date",y="n")+
  scale_fill_manual(values=c("red","orange") )+
  annotate("label", x=med1, y=3, label="m1",angle = 180,col="black",size=3.5,fill="white")+
  annotate("label", x=med2, y=3, label="m2",angle = 180,col="black",size=3.5,fill="white")


plot2


plot2.lab=ggplot()+
  geom_segment(aes(x=as.Date('0001-6-1'),y=0,xend=as.Date('0002-1-1'),yend=0))+
  geom_segment(aes(x=as.Date('0002-1-1'),xend=as.Date('0002-7-1'),y=0,yend=0))+
  geom_segment(aes(x=as.Date('0001-6-1'),xend=as.Date('0001-6-1'),y=-10,yend=10))+
  geom_segment(aes(x=as.Date('0002-1-1'),xend=as.Date('0002-1-1'),y=-10,yend=10))+
  geom_segment(aes(x=as.Date('0002-7-1'),xend=as.Date('0002-7-1'),y=-10,yend=10))+
  
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),panel.background = element_blank(),
        axis.ticks=element_blank(),axis.text=element_blank())+xlab("")+ylab("") +
  scale_x_date(breaks = date_breaks('2 months'),
               labels = date_format("%b"),
               limits = c(as.Date('0001-6-1'), as.Date('0002-7-1')))+
  ylim(-50,50)+
  annotate("label", x=as.Date('0001-9-15'), y=0, label="Year 1",angle = 90,col="black",size=3.5,fill="white")+
  annotate("label", x=as.Date('0002-4-1'), y=0, label="Year 2",angle = 90,col="black",size=3.5,fill="white")+ 
  theme_void()

plot2.final=grid.arrange(plot2,plot2.lab,ncol=1,nrow=2,heights=c(6,1))





Sys.setlocale("LC_TIME", "English")

plot_grid(plot2,plot4,labels=c("D","E"),ncol=2,nrow=1)  

df1=data.frame(Duree=tab_Expl_Resi2$Duree_1er_EXI[exploI])
df1$statut="Medium-dist."
df2=data.frame(Duree=tab_Expl_Resi2$Duree_1er_EXG[exploG])
df2$statut="Long-dist."
duree_df=na.omit(rbind(df1,df2))


plot4=ggplot(duree_df, aes(Duree, fill = statut)) + 
  geom_histogram(alpha = 1, aes(y = ..count..), position = 'identity',binwidth=10,center=10)+
  geom_vline(xintercept=median(df1$Duree,na.rm=T), linetype="dashed", color = "black") + 
  geom_vline(xintercept=median(df2$Duree,na.rm=T), linetype="dashed", color = "black")+ 
  xlim(0,100)+labs(x = "Duration (days)",title="  Exploration duration",y="n")+
  theme(plot.title = element_text(size=11,hjust = 0),axis.title.x=element_text(size=12),legend.position = "none",
        legend.title = element_blank(),legend.text=element_text(size=10),axis.title.y = element_blank(),
        axis.text.x = element_text(size=11))+
  scale_fill_manual(values=c("red","orange") )+
  annotate("label", x=median(df1$Duree,na.rm=T), y=3, label="m1",angle = 180,col="black",size=3.5,fill="white")+
  annotate("label", x=median(df2$Duree,na.rm=T), y=3, label="m2",angle = 180,col="black",size=3.5,fill="white")

plot4


plot_grid(pie3,plot2.final,plot4,labels=c("A","B","C"),ncol=3,nrow=1) 



#### REVIEW 2: Proportions statuts d'exploration en fonction des différents facteurs



# Traduction en anglais

data1$Exploration.Statut <- as.character(data1$Statut1ans)

data1$Exploration.Statut[which(data1$Exploration.Statut == "Explorateur intermediaire")] <- "Medium-distance explorers"
data1$Exploration.Statut[which(data1$Exploration.Statut == "Grand explorateur")] <- "Long-distance explorers"

data1$Exploration.Statut <- relevel(data1$Exploration.Statut, ref = "Long-distance explorers")

status_colors <- c("Resident" = "chartreuse3", "Medium-distance explorers" = "orange", "Long-distance explorers" = "red")

data1$Region <- as.character(data1$Site.1)
data1$Region[data1$Region == "Alpes externes"] <- "Prealps"
data1$Region[data1$Region == "Chaine de montagne"] <- "Mountains"
data1$Region[data1$Region == "Corse"] <- "Corsica"
data1$Region[data1$Region == "Massif Central"] <- "Causses"

data1$Region <- factor(data1$Region, levels = c("Causses", "Corsica", "Prealps","Mountains"))


data1$Birth <- as.character(data1$Statut.lacher)
data1$Birth[data1$Birth== "L"] <- "Released"
data1$Birth[data1$Birth== "NL"] <- "Wild born"

data1$Sexe <- as.character(data1$Sexe)
data1$Sexe[data1$Sexe== "m"] <- "Males"
data1$Sexe[data1$Sexe== "f"] <- "Females"

# Création d'une fonction pour extraire les étiquettes de pourcentage formatées
get_labels <- function(x) {
  paste0(round(x), "%")
}

# Calcul des pourcentages pour chaque combinaison
data1_summary1 <- data1 %>%
  group_by(Region, Exploration.Statut) %>%
  summarise(count = n()) %>%
  mutate(percentage = (count / sum(count)) * 100) %>%
  mutate(cum_percentage = rev(cumsum(rev(percentage))))


data1_summary2 <- data1 %>%
  group_by(Sexe, Exploration.Statut) %>%
  summarise(count = n()) %>%
  mutate(percentage = (count / sum(count)) * 100) %>%
  mutate(cum_percentage = rev(cumsum(rev(percentage))))

data1_summary3 <- data1 %>%
  group_by(Birth, Exploration.Statut) %>%
  summarise(count = n()) %>%
  mutate(percentage = (count / sum(count)) * 100) %>%
  mutate(cum_percentage = rev(cumsum(rev(percentage))))



colnames(data1_summary1)[1] <- "level"
colnames(data1_summary2)[1] <- "level"
colnames(data1_summary3)[1] <- "level"

data1_summary1$factor <- "Region of fledging"
data1_summary2$factor <- "Sex"
data1_summary3$factor <- "Birth origin"

data1_summary_tot <- rbind(rbind(data1_summary1,data1_summary2),data1_summary3)
data1_summary_tot$factor <- factor(data1_summary_tot$factor, levels=c("Region of fledging","Sex","Birth origin"))
data1_summary_tot$level <- factor(data1_summary_tot$level,
                                  levels=c("Causses","Prealps","Mountains","Corsica","Females","Males","Released","Wild born"))



# Création des barplots
# Ligne 1: Statut d'exploration en fonction de la région
plotProp <- ggplot(data1_summary_tot, aes(x = level, y = percentage, fill = Exploration.Statut)) +
  facet_grid(.~factor, scales = "free_x" ,space= "free_x")+
  geom_bar(stat = "identity", width = 0.35) +
  geom_text(aes(label = paste0(round(percentage), "%"), y = (cum_percentage-5)), 
            color = "white",fontface = "bold", size = 3.5) +
  scale_fill_manual(values = status_colors) +
  labs(y = "Explo. statuses (%)") +
  theme(legend.position = "None", axis.title.x = element_blank(), axis.text.x = element_text(size=10),
        axis.text.y = element_text(size= 10), axis.title.y =  element_text(size=12),
        strip.text = element_text(size=12))

plotProp



#### REVIEW 2: Age de départ en fonction des différents facteurs



# Traduction en anglais



data2$Region <- as.character(data2$Site.1)
data2$Region[data2$Region == "Alpes externes"] <- "Prealps"
data2$Region[data2$Region == "Chaine de montagne"] <- "Mountains"
data2$Region[data2$Region == "Corse"] <- "Corsica"
data2$Region[data2$Region == "Massif Central"] <- "Causses"

data2$Region <- factor(data2$Region, levels = c("Causses", "Corsica", "Prealps","Mountains"))


data2$Birth <- as.character(data2$Statut.lacher)
data2$Birth[data2$Birth== "L"] <- "Released"
data2$Birth[data2$Birth== "NL"] <- "Wild born"

data2$Sexe <- as.character(data2$Sexe)
data2$Sexe[data2$Sexe== "m"] <- "Males"
data2$Sexe[data2$Sexe== "f"] <- "Females"

# Création d'une fonction pour extraire les étiquettes de pourcentage formatées
get_labels <- function(x) {
  paste0(round(x), "%")
}

# Calcul des moyennes pour chaque combinaison
data2_summary1 <- data2 %>%
  group_by(Region) %>%
  summarise(mean_Age = mean(Age), sd= sd(Age), min = min(Age), max = max(Age)) 

data2_summary2 <- data2 %>%
  group_by(Sexe) %>%
  summarise(mean_Age = mean(Age), sd= sd(Age), min = min(Age), max = max(Age)) 

data2_summary3 <- data2 %>%
  group_by(Birth) %>%
  summarise(mean_Age = mean(Age), sd= sd(Age), min = min(Age), max = max(Age)) 


colnames(data2_summary1)[1] <- "level"
colnames(data2_summary2)[1] <- "level"
colnames(data2_summary3)[1] <- "level"

data2_summary1$factor <- "Region of fledging"
data2_summary2$factor <- "Sex"
data2_summary3$factor <- "Birth origin"

data2_summary_tot <- rbind(rbind(data2_summary1,data2_summary2),data2_summary3)
data2_summary_tot$factor <- factor(data2_summary_tot$factor, levels=c("Region of fledging","Sex","Birth origin"))
data2_summary_tot$level <- factor(data2_summary_tot$level,
                                  levels=c("Causses","Prealps","Mountains","Corsica","Females","Males","Released","Wild born"))



# Création des barplots
# Ligne 1: Statut d'exploration en fonction de la région

plotAge <- ggplot(data2_summary_tot, aes(x = level, y = mean_Age)) +
  geom_boxplot(aes(lower=mean_Age-sd,upper=mean_Age+sd,middle=mean_Age,
                   ymin=min,ymax=max),
               stat="identity",size=0.1,fill="skyblue4", width = 0.3) +
  facet_grid(.~factor, scales = "free_x" ,space= "free_x")+
  labs(y = "Departure Age \n (days after fledging)") +
  theme(legend.position = "None", axis.title.x = element_blank(), axis.text.x = element_text(size=10),
        axis.text.y = element_text(size= 10), axis.title.y =  element_text(size=12),
        strip.text = element_text(size=12))

plotAge


#### REVIEW 2: Date en fonction des différents facteurs



# Traduction en anglais


data3$Region <- as.character(data3$Site.1)
data3$Region[data3$Region == "Alpes externes"] <- "Prealps"
data3$Region[data3$Region == "Chaine de montagne"] <- "Mountains"
data3$Region[data3$Region == "Corse"] <- "Corsica"
data3$Region[data3$Region == "Massif Central"] <- "Causses"

data3$Region <- factor(data3$Region, levels = c("Causses", "Corsica", "Prealps","Mountains"))


data3$Birth <- as.character(data3$Statut.lacher)
data3$Birth[data3$Birth== "L"] <- "Released"
data3$Birth[data3$Birth== "NL"] <- "Wild born"

data3$Sexe <- as.character(data3$Sexe)
data3$Sexe[data3$Sexe== "m"] <- "Males"
data3$Sexe[data3$Sexe== "f"] <- "Females"

# Création d'une fonction pour extraire les étiquettes de pourcentDate formatées
get_labels <- function(x) {
  paste0(round(x), "%")
}

# Calcul des moyennes  pour chaque combinaison
data3_summary1 <- data3 %>%
  group_by(Region) %>%
  summarise(mean_Date = mean(date_julienne), sd= sd(date_julienne),min = min(date_julienne), max = max(date_julienne)) 

data3_summary2 <- data3 %>%
  group_by(Sexe) %>%
  summarise(mean_Date = mean(date_julienne), sd= sd(date_julienne),min = min(date_julienne), max = max(date_julienne)) 

data3_summary3 <- data3 %>%
  group_by(Birth) %>%
  summarise(mean_Date = mean(date_julienne), sd= sd(date_julienne),min = min(date_julienne), max = max(date_julienne)) 

colnames(data3_summary1)[1] <- "level"
colnames(data3_summary2)[1] <- "level"
colnames(data3_summary3)[1] <- "level"

data3_summary1$factor <- "Region of fledging"
data3_summary2$factor <- "Sex"
data3_summary3$factor <- "Birth origin"

data3_summary_tot <- rbind(rbind(data3_summary1,data3_summary2),data3_summary3)
data3_summary_tot$factor <- factor(data3_summary_tot$factor, levels=c("Region of fledging","Sex","Birth origin"))
data3_summary_tot$level <- factor(data3_summary_tot$level,
                                  levels=c("Causses","Prealps","Mountains","Corsica","Females","Males","Released","Wild born"))



# Création des barplots
# Ligne 1: Statut d'exploration en fonction de la région
plotDate <- ggplot(data3_summary_tot, aes(x = level, y = mean_Date)) +
  geom_boxplot(aes(lower=mean_Date-sd,upper=mean_Date+sd,middle=mean_Date,
                   ymin=min,ymax=max),
               stat="identity",size=0.1,fill="skyblue4", width = 0.3) +
  facet_grid(.~factor, scales = "free_x" ,space= "free_x")+
  labs(y = "Departure date \n (julian days)") +
  theme(legend.position = "None", axis.title.x = element_blank(), axis.text.x = element_text(size=10),
        axis.text.y = element_text(size= 10), axis.title.y =  element_text(size=12),
        strip.text = element_text(size=12))

plotDate




#### REVIEW 2: Duration en fonction des différents facteurs



# Traduction en anglais


data4$Region <- as.character(data4$Site.1)
data4$Region[data4$Region == "Alpes externes"] <- "Prealps"
data4$Region[data4$Region == "Chaine de montagne"] <- "Mountains"
data4$Region[data4$Region == "Corse"] <- "Corsica"
data4$Region[data4$Region == "Massif Central"] <- "Causses"

data4$Region <- factor(data4$Region, levels = c("Causses", "Corsica", "Prealps","Mountains"))


data4$Birth <- as.character(data4$Statut.lacher)
data4$Birth[data4$Birth== "L"] <- "Released"
data4$Birth[data4$Birth== "NL"] <- "Wild born"

data4$Sexe <- as.character(data4$Sexe)
data4$Sexe[data4$Sexe== "m"] <- "Males"
data4$Sexe[data4$Sexe== "f"] <- "Females"

# Création d'une fonction pour extraire les étiquettes de pourcentDuree formatées
get_labels <- function(x) {
  paste0(round(x), "%")
}

# Calcul des moyennes  pour chaque combinaison
data4_summary1 <- data4 %>%
  group_by(Region) %>%
  summarise(mean_Duree = mean(Duree), sd= sd(Duree),min = min(Duree), max = max(Duree)) 

data4_summary2 <- data4 %>%
  group_by(Sexe) %>%
  summarise(mean_Duree = mean(Duree), sd= sd(Duree),min = min(Duree), max = max(Duree)) 

data4_summary3 <- data4 %>%
  group_by(Birth) %>%
  summarise(mean_Duree = mean(Duree), sd= sd(Duree),min = min(Duree), max = max(Duree)) 


colnames(data4_summary1)[1] <- "level"
colnames(data4_summary2)[1] <- "level"
colnames(data4_summary3)[1] <- "level"

data4_summary1$factor <- "Region of fledging"
data4_summary2$factor <- "Sex"
data4_summary3$factor <- "Birth origin"

data4_summary_tot <- rbind(rbind(data4_summary1,data4_summary2),data4_summary3)
data4_summary_tot$factor <- factor(data4_summary_tot$factor, levels=c("Region of fledging","Sex","Birth origin"))
data4_summary_tot$level <- factor(data4_summary_tot$level,
                                  levels=c("Causses","Prealps","Mountains","Corsica","Females","Males","Released","Wild born"))

# Création des barplots
# Ligne 1: Statut d'exploration en fonction de la région

plotDuree <- ggplot(data4_summary_tot, aes(x = level, y = mean_Duree)) +
  geom_boxplot(aes(lower=mean_Duree-sd,upper=mean_Duree+sd,middle=mean_Duree,
                   ymin=min,ymax=max),
               stat="identity",size=0.1,fill="skyblue4", width = 0.3) +
  facet_grid(.~factor, scales = "free_x" ,space= "free_x")+
  labs(y = "Exploration \n duration (days)") +
  theme(legend.position = "None", axis.title.x = element_blank(), axis.text.x = element_text(size=10),
        axis.text.y = element_text(size= 10), axis.title.y =  element_text(size=12),
        strip.text = element_text(size=12))

plotDuree




#### REVIEW 2: Dmax-1y en fonction des différents facteurs



# Traduction en anglais


data5$Region <- as.character(data5$Site.1)
data5$Region[data5$Region == "Alpes externes"] <- "Prealps"
data5$Region[data5$Region == "Chaine de montagne"] <- "Mountains"
data5$Region[data5$Region == "Corse"] <- "Corsica"
data5$Region[data5$Region == "Massif Central"] <- "Causses"

data5$Region <- factor(data5$Region, levels = c("Causses", "Corsica", "Prealps","Mountains"))


data5$Birth <- as.character(data5$Statut.lacher)
data5$Birth[data5$Birth== "L"] <- "Released"
data5$Birth[data5$Birth== "NL"] <- "Wild born"

data5$Sexe <- as.character(data5$Sexe)
data5$Sexe[data5$Sexe== "m"] <- "Males"
data5$Sexe[data5$Sexe== "f"] <- "Females"

# Création d'une fonction pour extraire les étiquettes de pourcentDuree formatées
get_labels <- function(x) {
  paste0(round(x), "%")
}

# Calcul des moyennes  pour chaque combinaison
data5_summary1 <- data5 %>%
  group_by(Region) %>%
  summarise(mean_Dmax = mean(Dmax.1y), sd= sd(Dmax.1y),min = min(Dmax.1y), max = max(Dmax.1y)) 

data5_summary2 <- data5 %>%
  group_by(Sexe) %>%
  summarise(mean_Dmax = mean(Dmax.1y), sd= sd(Dmax.1y),min = min(Dmax.1y), max = max(Dmax.1y)) 

data5_summary3 <- data5 %>%
  group_by(Birth) %>%
  summarise(mean_Dmax = mean(Dmax.1y), sd= sd(Dmax.1y),min = min(Dmax.1y), max = max(Dmax.1y)) 

colnames(data5_summary1)[1] <- "level"
colnames(data5_summary2)[1] <- "level"
colnames(data5_summary3)[1] <- "level"

data5_summary1$factor <- "Region of fledging"
data5_summary2$factor <- "Sex"
data5_summary3$factor <- "Birth origin"

data5_summary_tot <- rbind(rbind(data5_summary1,data5_summary2),data5_summary3)
data5_summary_tot$factor <- factor(data5_summary_tot$factor, levels=c("Region of fledging","Sex","Birth origin"))
data5_summary_tot$level <- factor(data5_summary_tot$level,
                                  levels=c("Causses","Prealps","Mountains","Corsica","Females","Males","Released","Wild born"))

# Création des barplots
# Ligne 1: Statut d'exploration en fonction de la région
plotDmax <- ggplot(data5_summary_tot, aes(x = level, y = mean_Dmax)) +
  geom_boxplot(aes(lower=mean_Dmax-sd,upper=mean_Dmax+sd,middle=mean_Dmax,
                   ymin=min,ymax=max),
               stat="identity",size=0.1,fill="skyblue4", width = 0.3) +
  facet_grid(.~factor, scales = "free_x" ,space= "free_x")+
  labs(y = "Dmax-1y (km)") +
  theme(legend.position = "None", axis.title.x = element_blank(), axis.text.x = element_text(size=10),
        axis.text.y = element_text(size= 10), axis.title.y =  element_text(size=12),
        strip.text = element_text(size=12))

plotDmax

# Organiser les graphiques 

plot_grid(plotProp, plotDmax, plotDate , plotDuree, ncol=1) 
