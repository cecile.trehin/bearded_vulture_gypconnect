#
#     ANALYSE GYPA : Figure segmentation Layrou (Appendix 3 ESM)  
#     @C.Trehin 2018
#___________________________________________________________________________
library(png)


load("~/Gypa/Scripts et figures modifiés gypa/nsd_layrou.RData")
load("~/Gypa/Scripts et figures modifiés gypa/layrou_seg.RData")
load("~/Gypa/Scripts et figures modifiés gypa/layrou.data.RData")

img <- readPNG("C:/Users/cetrehin/Documents/Gypa/Layrou.png")


## trajet



g1 = ggplot(iris, aes(Species, Sepal.Length))+
  annotation_custom(rasterGrob(img, width=unit(1,"npc"), height=unit(1,"npc")), 
                    -Inf, Inf, -Inf, Inf) +
  labs(title="") +
  theme(plot.title = element_text(size=15,hjust=0),
        axis.text.y=element_blank(),
        axis.title.x=element_blank(),
        axis.title.y=element_blank())
g1

## NSD
layrou$outputs

#medium: 0002-04-05 ? 0002-04-23  /  0002-05-05 ? 0002-05-09
#long: 0002-05-23 ? 0002-06-06
which(nsd_layrou$time==as.Date("0002-04-05")) #184
which(nsd_layrou$time==as.Date("0002-04-23")) #198
which(nsd_layrou$time==as.Date("0002-05-05")) #206
which(nsd_layrou$time==as.Date("0002-05-09")) #210
which(nsd_layrou$time==as.Date("0002-05-23")) #224
which(nsd_layrou$time==as.Date("0002-06-06")) #238
which(nsd_layrou$time==as.Date('0003-1-1')) #379


g2 = ggplot() + 
  annotate("rect",xmin=nsd_layrou$time[1], xmax=nsd_layrou$time[184],
           ymin=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)-40000, ymax=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)+40000, 
           fill="chartreuse3", alpha=0.8)+
  annotate("rect",xmin=nsd_layrou$time[184], xmax=nsd_layrou$time[198], 
           ymin=median(nsd_layrou$NSD[c(184:198,206:210)]/1000000)-40000, ymax=median(nsd_layrou$NSD[c(184:198,206:210)]/1000000)+40000, 
           fill="orange", alpha=0.8)+
  annotate("rect",xmin=nsd_layrou$time[198], xmax=nsd_layrou$time[206], 
           ymin=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)-40000, ymax=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)+40000, 
           fill="chartreuse3", alpha=0.8)+
  annotate("rect",xmin=nsd_layrou$time[206], xmax=nsd_layrou$time[210], 
           ymin=median(nsd_layrou$NSD[c(184:198,206:210)]/1000000)-40000, ymax=median(nsd_layrou$NSD[c(184:198,206:210)]/1000000)+40000, 
           fill="orange", alpha=0.8)+
  annotate("rect",xmin=nsd_layrou$time[210], xmax=nsd_layrou$time[224], 
           ymin=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)-40000, ymax=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)+40000, 
           fill="chartreuse3", alpha=0.8)+
  annotate("rect",xmin=nsd_layrou$time[224], xmax=nsd_layrou$time[238], 
           ymin=median(nsd_layrou$NSD[224:238]/1000000)-40000, ymax=median(nsd_layrou$NSD[224:238]/1000000)+40000, 
           fill="red", alpha=0.8)+
  annotate("rect",xmin=nsd_layrou$time[238], xmax=nsd_layrou$time[379], 
           ymin=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)-40000, ymax=median(nsd_layrou$NSD[c(1:184,198:206,210:224,238:379)]/1000000)+40000, 
           fill="chartreuse3", alpha=0.8)+
  theme_bw() +
  labs(title="",y="NSD (km2)") +
  scale_x_date(breaks = date_breaks('2 months'),
               labels = date_format("%y-%b"),
               limits = c(as.Date('0001-5-1'), as.Date('0003-1-1'))) + 
  theme(axis.text.x=element_text(angle=45,hjust=0.5,vjust=0.5,size=10),
        axis.title.x=element_blank(),plot.title = element_text(size=15,face="bold",hjust = 0)) +
  coord_cartesian(ylim = c(0,500000))+
  guides(colour=FALSE)+
  scale_y_continuous(labels=comma)+
  geom_hline(yintercept=40000, linetype="dashed", color = "black") + 
  geom_hline(yintercept=120000, linetype="dashed", color = "black")+
  annotate("label", x=as.Date('0001-5-1'), y=40000, label="B1",angle = 90,col="black",size=3.5,fill="white")+
  annotate("label", x=as.Date('0001-5-1'), y=120000, label="B2",angle = 90,col="black",size=3.5,fill="white")+
  geom_line(aes(x=nsd_layrou$time,y=nsd_layrou$NSD/1000000),color='black') 


g2


g2.lab=ggplot()+
  geom_segment(aes(x=as.Date('0001-5-1'),y=0,xend=as.Date('0002-1-1'),yend=0))+
  geom_segment(aes(x=as.Date('0002-1-1'),xend=as.Date('0003-1-1'),y=0,yend=0))+
  geom_segment(aes(x=as.Date('0001-5-1'),xend=as.Date('0001-5-1'),y=-10,yend=10))+
  geom_segment(aes(x=as.Date('0002-1-1'),xend=as.Date('0002-1-1'),y=-10,yend=10))+
  geom_segment(aes(x=as.Date('0003-1-1'),xend=as.Date('0003-1-1'),y=-10,yend=10))+
  
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),panel.background = element_blank(),
        axis.ticks=element_blank(),axis.text=element_blank())+xlab("")+ylab("") +
  scale_x_date(breaks = date_breaks('2 months'),
               labels = date_format("%b"),
               limits = c(as.Date('0001-5-1'), as.Date('0003-1-1')))+
  ylim(-50,50)+
  annotate("label", x=as.Date('0001-9-1'), y=0, label="Year 1",angle = 90,col="black",size=3.5,fill="white")+
  annotate("label", x=as.Date('0002-6-15'), y=0, label="Year 2",angle = 90,col="black",size=3.5,fill="white")+ 
  theme_void()

g2.final=grid.arrange(g2,g2.lab,ncol=1,nrow=2,heights=c(6,1))



### segmentation

# remplacement nom Dnuitee3j par Dx
colnames(layrou$data)[4]="Dx (km)"
layrou$`Segmented variables`="Dx (km)"
colnames(layrou$outputs$`2 class - 13 segments`$states)[c(3,4)]=c("mu.Dx (km)","sd.Dx (km)")
layrou$`Diagnostic variables`= "Dx (km)"

# passage en km
layrou$data$`Dx (km)`= layrou$data$`Dx (km)`/ 1000
layrou$outputs$`2 class - 13 segments`$states$`mu.Dx (km)`=layrou$outputs$`2 class - 13 segments`$states$`mu.Dx (km)`/1000
layrou$outputs$`2 class - 13 segments`$states$`sd.Dx (km)`=layrou$outputs$`2 class - 13 segments`$states$`sd.Dx (km)`/1000


g3 = plot(layrou)+
  theme_bw() +
  scale_fill_manual(values=c("chartreuse3","orange","red"),
                    labels = c("Sedentary", "Medium-dist.","Long-dist."),alpha=0.1)+
  scale_colour_manual(values=c("chartreuse3","orange","red"),alpha=0.1) + 
  labs(title="Segmentation method (Dx3)",y="Dx3 (km)",x="location index",fill ="Behavior") +
  theme(plot.title = element_text(size=15,face="bold",hjust = 0),legend.position = c(0.25,0.8),
        strip.text.x = element_blank(),legend.title = element_blank())+
  coord_cartesian(xlim = c(0,380))+
  guides(colour=FALSE)

g3

g4 = segmap(layrou)+geom_point(size=2,aes(color=factor(state)))+
  theme_bw() +
  scale_color_manual(aes(color=factor(state)),values=c("chartreuse3","orange","red"),
                      labels = c("Sedentary", "Medium-dist.","Long-dist.")) +
  labs(title="",colour ="Behavior")+
  theme(plot.title = element_text(size=15,face="bold",hjust = 0),legend.position = "none",legend.title = element_blank())
g4

### refaire la figure Dmax 

layrou.data$Dnuitee3j.km = layrou.data$Dnuitee3j/1000

#medium: 0002-04-05 ? 0002-04-23  /  0002-05-05 ? 0002-05-09
#long: 0002-05-23 ? 0002-06-06
which(as.Date(layrou.data$time)==as.Date("0002-04-05")) #199
which(as.Date(layrou.data$time)==as.Date("0002-04-23")) #213
which(as.Date(layrou.data$time)==as.Date("0002-05-05")) #223
which(as.Date(layrou.data$time)==as.Date("0002-05-09")) #227
which(as.Date(layrou.data$time)==as.Date("0002-05-23")) #241
which(as.Date(layrou.data$time)==as.Date("0002-06-06")) #255
which(as.Date(layrou.data$time)==as.Date('0003-1-1')) #400

g5 = ggplot() + 
  theme_bw() +
  labs(title="",y="Dx3 (km)") +
  scale_x_date(breaks = date_breaks('2 months'),
               labels = date_format("%b"),
               limits = c(as.Date('0001-5-1'), as.Date('0003-1-1'))) + 
  theme(axis.text.x=element_text(angle=45,hjust=0.5,vjust=0.5,size=10),
        axis.title.x=element_blank(),plot.title = element_text(size=15,face="bold",hjust = 0)) +
  guides(colour=FALSE)+
  scale_y_continuous(labels=comma)+
  annotate("rect",xmin=as.Date(layrou.data$time)[1], xmax=as.Date(layrou.data$time)[199],
           ymin=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)+50, 
           fill="chartreuse3", alpha=0.8)+
  annotate("rect",xmin=as.Date(layrou.data$time)[199], xmax=as.Date(layrou.data$time)[213], 
           ymin=median(layrou.data$Dnuitee3j.km[c(199:213,223:227)],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[c(199:213,223:227)],na.rm=T)+50, 
           fill="orange", alpha=0.8)+
  annotate("rect",xmin=as.Date(layrou.data$time)[213], xmax=as.Date(layrou.data$time)[223], 
           ymin=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)+50, 
           fill="chartreuse3", alpha=0.8)+
  annotate("rect",xmin=as.Date(layrou.data$time)[223], xmax=as.Date(layrou.data$time)[227], 
           ymin=median(layrou.data$Dnuitee3j.km[c(199:213,223:227)],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[c(199:213,223:227)],na.rm=T)+50, 
           fill="orange", alpha=0.8)+
  annotate("rect",xmin=as.Date(layrou.data$time)[227], xmax=as.Date(layrou.data$time)[241], 
           ymin=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)+50, 
           fill="chartreuse3", alpha=0.8)+
  annotate("rect",xmin=as.Date(layrou.data$time)[241], xmax=as.Date(layrou.data$time)[255], 
           ymin=median(layrou.data$Dnuitee3j.km[241:255],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[241:255],na.rm=T)+50, 
           fill="red", alpha=0.8)+
  annotate("rect",xmin=as.Date(layrou.data$time)[255], xmax=as.Date(layrou.data$time)[400], 
           ymin=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)-50, ymax=median(layrou.data$Dnuitee3j.km[c(1:199,213:223,227:241,255:400)],na.rm=T)+50, 
           fill="chartreuse3", alpha=0.8)+
  geom_hline(yintercept=150, linetype="dashed", color = "black") + 
  geom_hline(yintercept=300, linetype="dashed", color = "black")+
  annotate("label", x=as.Date('0001-5-1'), y=150, label="A1",angle = 90,col="black",size=3.5,fill="white")+
  annotate("label", x=as.Date('0001-5-1'), y=300, label="A2",angle = 90,col="black",size=3.5,fill="white")+
  geom_line(aes(x=as.Date(layrou.data$time),y=layrou.data$Dnuitee3j.km),color='black')#+
  #ylim=c(0,500)#+
  #annotation_custom("a",xmin=as.Date('0001-5-1'),xmax=as.Date('0001-6-1'),ymin=-0.07,ymax=-0.07)

g5 

g5.lab=ggplot()+
  geom_segment(aes(x=as.Date('0001-5-1'),y=0,xend=as.Date('0002-1-1'),yend=0))+
  geom_segment(aes(x=as.Date('0002-1-1'),xend=as.Date('0003-1-1'),y=0,yend=0))+
  geom_segment(aes(x=as.Date('0001-5-1'),xend=as.Date('0001-5-1'),y=-10,yend=10))+
  geom_segment(aes(x=as.Date('0002-1-1'),xend=as.Date('0002-1-1'),y=-10,yend=10))+
  geom_segment(aes(x=as.Date('0003-1-1'),xend=as.Date('0003-1-1'),y=-10,yend=10))+
  
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),panel.background = element_blank(),
        axis.ticks=element_blank(),axis.text=element_blank())+xlab("")+ylab("") +
  scale_x_date(breaks = date_breaks('2 months'),
               labels = date_format("%b"),
               limits = c(as.Date('0001-5-1'), as.Date('0003-1-1')))+
  ylim(-50,50)+
  annotate("label", x=as.Date('0001-9-1'), y=0, label="Year 1",angle = 90,col="black",size=3.5,fill="white")+
  annotate("label", x=as.Date('0002-6-15'), y=0, label="Year 2",angle = 90,col="black",size=3.5,fill="white")+ 
  theme_void()

g5.final=grid.arrange(g5,g5.lab,ncol=1,nrow=2,heights=c(6,1))

### plot toutes les figures

plot_grid(g1,g2,labels=c("A","B"),ncol=2,nrow=1) 
plot_grid(g1,g2.final,g4,g5.final,labels=c("A","B","C","D"),ncol=2,nrow=2) 
